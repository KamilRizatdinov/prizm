import asyncio
from aiogram import Bot, Dispatcher, executor 

from config import BOT_TOKEN, ADMIN_IDS, TEST_WALLET


bot = Bot(token=BOT_TOKEN, parse_mode='HTML')
dp = Dispatcher(bot=bot)
loop = asyncio.get_event_loop()


@dp.message_handler(commands=['start'])
async def start_handler(msg):
    await bot.send_message(msg.chat.id, 'hello')


loop.run_until_complete(executor.start_polling(dp))
