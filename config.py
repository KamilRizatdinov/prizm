import os
import re

BOT_TOKEN = os.getenv('BOT_TOKEN')
TEST_WALLET = os.getenv('WALLET')
ADMIN_IDS = re.findall('\d+', os.getenv('ADMIN_IDS', ''))
HOST_URL = os.getenv('HOST_URL')
DB_NAME = os.getenv('DB_NAME')
