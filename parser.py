import aiogram
import aiohttp
import asyncio
from bs4 import BeautifulSoup

URL = 'https://prizmbank.ru/?{}'.format()


async def fetch(client):
    async with client.get(URL) as resp:
        return await resp.text()


async def get_tnx(tnx_table):
    tnx_rows = tnx_table.find_all('tr')
    tnx = []

    for tnx_row in tnx_rows[1:]:
        tnx_data = tnx_row.find_all('font')
        tnx_info = {}
        amount = ' '.join(tnx_data[2].text.split())

        if '-' in amount:
            tnx_info['from'] = ''
            tnx_info['to'] = ' '.join(tnx_data[1].text.split())
        else:
            tnx_info['from'] = ' '.join(tnx_data[1].text.split())
            tnx_info['to'] = ''

        tnx_info['date'] = ' '.join(tnx_data[0].text.split())
        tnx_info['amount'] = amount

        tnx.append(tnx_info)

    return tnx


async def get_wallet_info(wallet_table):
    wallet_rows = wallet_table.find_all('tr')
    info = {}
    wallet_data = []

    for wallet_row in wallet_rows[1:]:
        for wallet_cell in wallet_row.find_all('font'):
            wallet_data.append(' '.join(wallet_cell.text.split()))

    info['price_usd'] = wallet_data[0]
    info['price_rub'] = wallet_data[1]
    info['balance_prizm'] = wallet_data[2]
    info['balance_rub'] = wallet_data[3]
    info['structure'] = wallet_data[4]
    info['daily_percent'] = wallet_data[5]
    info['daily_rub'] = wallet_data[6]
    info['paramining'] = wallet_data[7]
    info['daily_tnx'] = wallet_data[9]
    info['daily_volume'] = wallet_data[10]
    info['rang'] = wallet_data[11]
    info['amount'] = wallet_data[12]
    info['btc-eth'] = wallet_data[13]

    return info


async def parse():
    async with aiohttp.ClientSession() as client:
        result = {}
        html = await fetch(client)
        soup = BeautifulSoup(html, 'html.parser')
        await asyncio.sleep(0)
        tables = soup.find_all('table')
        wallet_table = tables[0]
        tnx_table = tables[2]

        result['info'] = await get_wallet_info(wallet_table)
        result['tnx'] = await get_tnx(tnx_table)
        return result
