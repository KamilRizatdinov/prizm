import asyncio

from motor import motor_asyncio

from db.models.user import instance, User
from config import HOST_URL, DB_NAME 


async def update_user(chat_id, **kwargs):
    user = await User.find_one({"chat_id": chat_id})

    if user is None:
        new_user = User(chat_id=chat_id, **kwargs)
        await new_user.commit()
    else:
        for k, v in kwargs.items():
            setattr(user, k, v)
        await user.commit()


async def get_user(chat_id):
    return await User.find_one({"chat_id": chat_id})


async def drop_db():
    await User.collection.drop()


client = motor_asyncio.AsyncIOMotorClient(HOST_URL)
instance.init(client[database.DB_NAME])
asyncio.get_event_loop().create_task(User.ensure_indexes())
